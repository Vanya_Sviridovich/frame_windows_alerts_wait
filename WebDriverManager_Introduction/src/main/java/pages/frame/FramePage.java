package pages.frame;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.Base_page;

import java.util.concurrent.TimeUnit;

public class FramePage extends Base_page {

    public final String url = "http://jsbin.com/?html,output";
    JavascriptExecutor js = (JavascriptExecutor) _driver;

    private final String tag = "<input id=\"test\"/> ";

    @FindBy(how = How.XPATH, using = "//iframe[@class='stretch']")
    private WebElement outerFrame;

    @FindBy(how = How.XPATH, using = "//div[@id='sandbox-wrapper']/iframe")
    private WebElement innerFrame;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'activeline')]//span/parent::pre/span")
    private WebElement editField;

    @FindBy(how = How.ID, using = "test")
    public WebElement input;


//    @FindBy(how = How.TAG_NAME, using = "title")
//    public WebElement mainPageTitle;
//
//
//    @FindBy(how = How.TAG_NAME, using = "title")
//    public WebElement outerFrameTitle;
//
//    @FindBy(how = How.TAG_NAME, using = "title")
//    public WebElement innerFrameTitle;

    public FramePage(WebDriver driver) {
        super(driver);
    }

    public void fillOutInput(String text) {

        new Actions(_driver).sendKeys(editField, tag).build().perform();
        //_driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        _wait.until(d -> {
            d.switchTo().defaultContent();
            d.switchTo().frame(outerFrame);
            d.switchTo().frame(innerFrame);
            System.out.println(_driver.getPageSource());
            return d.findElements(By.id("test")).size() > 0;
        });

        input.sendKeys(text);
    }

    public String getInputValue() {
        return (String) js.executeScript("return arguments[0].value", input);
    }
}
