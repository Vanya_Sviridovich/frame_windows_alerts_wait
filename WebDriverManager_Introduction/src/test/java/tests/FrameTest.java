package tests;

import org.junit.Assert;
import org.junit.Test;
import pages.frame.FramePage;

public class FrameTest extends BaseTest {

    private final String expectedValue = "Hello";

    @Test
    public void inputText() {
        FramePage framePage = new FramePage(_driver.get_driver());

        framePage.openURL(framePage.url);
        framePage.fillOutInput(expectedValue);
        Assert.assertEquals("values aren't the same", framePage.getInputValue(), expectedValue);
    }
}
